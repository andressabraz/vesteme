import { LookModalPage } from './look-modal/look-modal.page';
import { FormsModule } from '@angular/forms';
import { Camera } from '@ionic-native/camera/ngx';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore'
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Facebook } from '@ionic-native/facebook/ngx'
import { ImageModalPage } from './image-modal/image-modal.page';
import { LookModalPageModule } from './look-modal/look-modal.module';

@NgModule({
  declarations: [AppComponent, ImageModalPage, LookModalPage],
  entryComponents: [ImageModalPage, LookModalPage],
  imports: [BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    FormsModule
  ],
  providers: [
    StatusBar,
    Facebook,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Keyboard,
    ImagePicker,
    Camera
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
