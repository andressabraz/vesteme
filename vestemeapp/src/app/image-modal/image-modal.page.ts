import { DataService } from './../data.service';
import { Component, OnInit, Input } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { ImageService } from '../image.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-image-modal',
  templateUrl: './image-modal.page.html',
  styleUrls: ['./image-modal.page.scss'],
})
export class ImageModalPage implements OnInit {

  categoriasArray= ["Blusas e cia", "Calças e cia", "Vestidos e cia", "Sapatos e cia", "Acessórios e cia"];
  categorias = {"Blusas e cia": ["Blusas", "Casacos", "Outros"], 
  "Calças e cia": ["Calças", "Saias", "Outros"], 
  "Vestidos e cia": ["Vestidos", "Macacões", "Outros"], 
  "Sapatos e cia": ["Sapatos", "Tênis", "Sándalias", "Botas", "Outros"], 
  "Acessórios e cia": ["Bolsas", "Lenços", "Jóias/Bijus", "Outros"]};
  categoriaEscolhida;
  subcategoriaEscolhida;
  imageName;
  imageUrl;
  constructor(navParams: NavParams, public imageService: ImageService, public dataService: DataService,
    private router: Router, private modalCtrl: ModalController) {
    this.categoriaEscolhida=navParams.get('img').categoria;
    this.subcategoriaEscolhida=navParams.get('img').subcategoria;
    this.imageName=navParams.get('img').nome;
    this.imageUrl=navParams.get('img').imageUrl;
  }

  ngOnInit() {

  }

  salvar() {

    let newImage={categoria: this.categoriaEscolhida, subcategoria: this.subcategoriaEscolhida, nome: this.imageName, imageUrl: this.imageUrl}
    this.dataService.updateImage(newImage).then(()=>{
      this.dismiss();
      this.dataService.getImagesFromUser();
      this.router.navigate(['home']);
    });
  }

  deleteImage() {
    this.dataService.deleteImage(this.imageUrl).then(()=>{
      this.dismiss();
      this.dataService.getImagesFromUser();
      this.router.navigate(['home']);
    })
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}
