import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LookModalPage } from './look-modal.page';

describe('LookModalPage', () => {
  let component: LookModalPage;
  let fixture: ComponentFixture<LookModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LookModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LookModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
