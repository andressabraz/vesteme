import { DataService } from './../data.service';
import { Component, OnInit, Input } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { ImageService } from '../image.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-look-modal',
  templateUrl: './look-modal.page.html',
  styleUrls: ['./look-modal.page.scss'],
})
export class LookModalPage implements OnInit {

  categoriasArray= ["Formal", "Casual", "Glamouroso", "Viagens", "Outros"];
  categoriaEscolhida;
  imageName;
  imageUrl;
  constructor(navParams: NavParams, public imageService: ImageService, public dataService: DataService,
    private router: Router, private modalCtrl: ModalController) {
    this.categoriaEscolhida=navParams.get('img').categoria;
    this.imageName=navParams.get('img').nome;
    this.imageUrl=navParams.get('img').imageUrl;
  }

  ngOnInit() {

  }

  salvar() {

    let newImage={categoria: this.categoriaEscolhida, nome: this.imageName, imageUrl: this.imageUrl}
    this.dataService.updateLook(newImage).then(()=>{
      this.dismiss();
      this.dataService.getImagesFromUser();
      this.router.navigate(['home']);
    });
  }

  deleteLook() {
    console.log("deleting "+this.imageUrl)
    this.dataService.deleteLook(this.imageUrl).then(()=>{
      this.dismiss();
      this.dataService.getImagesFromUser();
      this.router.navigate(['home']);
    })
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}
