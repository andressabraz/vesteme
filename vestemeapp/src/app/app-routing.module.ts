import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule', canActivate: [AuthGuard]},
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule', canActivate: [LoginGuard]},
  { path: 'contatos', loadChildren: './pages/contatos/contatos.module#ContatosPageModule', canActivate: [AuthGuard]},
  { path: 'upload', loadChildren: './pages/upload/upload.module#UploadPageModule', canActivate: [AuthGuard]},
  { path: 'meucloset', loadChildren: './pages/meucloset/meucloset.module#MeuclosetPageModule', canActivate: [AuthGuard]},
  { path: 'meulook', loadChildren: './pages/meulook/meulook.module#MeulookPageModule', canActivate: [AuthGuard]},
  { path: 'addlook', loadChildren: './addlook/addlook.module#AddlookPageModule', canActivate: [AuthGuard]},  { path: 'image-modal', loadChildren: './image-modal/image-modal.module#ImageModalPageModule' },
  { path: 'look-modal', loadChildren: './look-modal/look-modal.module#LookModalPageModule' }



];


@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }


