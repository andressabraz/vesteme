import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddlookPage } from './addlook.page';

describe('AddlookPage', () => {
  let component: AddlookPage;
  let fixture: ComponentFixture<AddlookPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddlookPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddlookPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
