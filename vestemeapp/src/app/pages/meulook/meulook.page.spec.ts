import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeulookPage } from './meulook.page';

describe('MeulookPage', () => {
  let component: MeulookPage;
  let fixture: ComponentFixture<MeulookPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeulookPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeulookPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
