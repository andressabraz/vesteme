import { LookModalPage } from './../../look-modal/look-modal.page';
import { Router } from '@angular/router';
import { LookService } from './../../look.service';
import { ImageService } from './../../image.service';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { IonSearchbar, ModalController } from '@ionic/angular';
import { ImageModalPage } from 'src/app/image-modal/image-modal.page';


@Component({
  selector: 'app-meulook',
  templateUrl: './meulook.page.html',
  styleUrls: ['./meulook.page.scss'],
})
export class MeulookPage implements OnInit {
  
  @ViewChild('search') search :IonSearchbar;
  canvas: any;
  categorias = ["Formal", "Casual", "Glamouroso", "Viagens", "Outros"];
  categoriaEscolhida;
  imagesToShow=[];
  filteredImages=[];
  isSearching=false;
  modalStarted;
  constructor(private imgService: ImageService, private lookService: LookService, private router: Router,
    private modalController: ModalController) { }

  escolherCategoria(categoria) {
    this.categoriaEscolhida=categoria;
    this.imagesToShow=[]
    this.lookService.meusLooks.forEach(look => {
      if (look.categoria==this.categoriaEscolhida) {
        this.imagesToShow.push(look);
      }
    });
  }



  ngOnInit() {
    
    //this.canvas.add(fabric.Image.fromURL(this.imgService.images[0].data));
  }

  hideSearchBar() {
    this.isSearching=false;
  }
  updateSearch(newValue) {
    console.log(newValue);
    this.filteredImages=[];
    this.lookService.meusLooks.forEach(image => {
      if (image.nome.toLowerCase().includes(newValue.toLowerCase())) {
        this.filteredImages.push(image)
      }
    });
    console.log(this.filteredImages);
  }
  clickedSearch() {

    this.isSearching=true;
    setTimeout(() => {
      this.search.setFocus();
    }, 100);
  }
  goBack() {
    if (this.categoriaEscolhida) {
      this.categoriaEscolhida='';
      return;
    }
    this.router.navigate(['home'])
  }
  async showModal(data) {
    this.modalStarted=true;
    console.log(data)
    try{
    const modal = await this.modalController.create({
      component: LookModalPage,
      backdropDismiss: true,
      showBackdrop: true,
      cssClass: 'modal-style',
      componentProps: {img: data}
    });
    return await modal.present();
    } 
    catch(e) {
      console.log(e)
    }
  }
}
