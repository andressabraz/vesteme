import { Router } from '@angular/router';
import { ImageService } from './../../image.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { ImageModalPage } from 'src/app/image-modal/image-modal.page';

@Component({
  selector: 'app-meucloset',
  templateUrl: './meucloset.page.html',
  styleUrls: ['./meucloset.page.scss'],
})
export class MeuclosetPage implements OnInit {
  @ViewChild('search') search :IonSearchbar;
  categoriasArray= ["Blusas e cia", "Calças e cia", "Vestidos e cia", "Sapatos e cia", "Acessórios e cia"];
  categorias = {"Blusas e cia": ["Blusas", "Casacos", "Outros"], 
  "Calças e cia": ["Calças", "Saias", "Shorts", "Outros"], 
  "Vestidos e cia": ["Vestidos", "Macacões", "Outros"], 
  "Sapatos e cia": ["Sapatos", "Tênis", "Sándalias", "Botas", "Outros"], 
  "Acessórios e cia": ["Bolsas", "Lenços", "Jóias/Bijus", "Outros"]};
  categoriaEscolhida='';
  subcategoriaEscolhida='';
  imagesToShow=[];
  isSearching=false;
  searchText;
  filteredImages=[];
  modalStarted=false;
  constructor(private imgService: ImageService, private router: Router, public modalController: ModalController) { }

  ngOnInit() {
    console.log(this.imgService.images)
  }

  escolherCategoria(categoria) {
    this.categoriaEscolhida=categoria;
  }
  escolherSubcategoria(subcategoria){
    this.subcategoriaEscolhida=subcategoria;
    this.imagesToShow=[];
    if (this.imgService.images) {
      this.imgService.images.forEach(image => {
        if (image.categoria==this.categoriaEscolhida && image.subcategoria==this.subcategoriaEscolhida){
          this.imagesToShow.push(image);
        }
      });
    } 
  }

  clickedSearch() {
    this.updateSearch("");
    this.isSearching=true;
    setTimeout(() => {
      this.search.setFocus();
    }, 100);
  }

  updateSearch(newValue) {
    this.filteredImages=[];
    this.imgService.images.forEach(image => {
      if (image.nome.toLowerCase().includes(newValue.toLowerCase())) {
        this.filteredImages.push(image)
      }
    });
  }

  goBack() {
    if (this.isSearching) {
      this.isSearching=false;
      return;
    }
    if (this.subcategoriaEscolhida!='') {
      this.subcategoriaEscolhida='';
      return;
    }
    if (this.categoriaEscolhida!='') {
      this.categoriaEscolhida='';
      return;
    }
    this.router.navigate(['home']);
  }

  async showModal(data) {
    this.modalStarted=true;
    console.log(data)
    try{
    const modal = await this.modalController.create({
      component: ImageModalPage,
      backdropDismiss: true,
      showBackdrop: true,
      cssClass: 'modal-style',
      componentProps: {img: data}
    });
    return await modal.present();
    } 
    catch(e) {
      console.log(e)
    }
  }
}
