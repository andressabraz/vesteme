import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MeuclosetPage } from './meucloset.page';
import { ImageModalPage } from 'src/app/image-modal/image-modal.page';

const routes: Routes = [
  {
    path: '',
    component: MeuclosetPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MeuclosetPage]
})
export class MeuclosetPageModule {}
