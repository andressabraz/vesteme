import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeuclosetPage } from './meucloset.page';

describe('MeuclosetPage', () => {
  let component: MeuclosetPage;
  let fixture: ComponentFixture<MeuclosetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeuclosetPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeuclosetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
