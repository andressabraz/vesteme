import { AngularFirestore } from '@angular/fire/firestore';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';
import * as firebase from 'firebase';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  private loading: any;

  private productsSubscription: Subscription;
  selected;
  imageResponse=[];
  usersCollection;
  images;
  constructor(
    private authService: AuthService,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private imagePicker: ImagePicker,
    private camera: Camera,
    private router: Router,
    private dataService: DataService,
    private afs: AngularFirestore
  ) {

  }
  ngOnInit() {
    this.usersCollection = this.afs.collection('user');

    let userId=firebase.auth().currentUser.uid;
    console.log(userId);
    
    this.selected='home';
    /*
    this.dataService.getImagesFromUser().then(data=>{
      console.log(data);
    })
    */
    
  }
  logout() {
    this.authService.logout().then(()=>{
        this.router.navigate(['login']);
    });
  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Aguarde...' });
    return this.loading.present();
  }
} 
