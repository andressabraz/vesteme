import { DataService } from 'src/app/data.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { ImageService } from './../../image.service';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.page.html',
  styleUrls: ['./upload.page.scss'],
})
export class UploadPage implements OnInit {
  selected;
  imageResponse=[];
  categoriasArray= ["Blusas e cia", "Calças e cia", "Vestidos e cia", "Sapatos e cia", "Acessórios e cia"];
  categorias = {"Blusas e cia": ["Blusas", "Casacos", "Outros"], 
  "Calças e cia": ["Calças", "Saias", "Outros"], 
  "Vestidos e cia": ["Vestidos", "Macacões", "Outros"], 
  "Sapatos e cia": ["Sapatos", "Tênis", "Sándalias", "Botas", "Outros"], 
  "Acessórios e cia": ["Bolsas", "Lenços", "Jóias/Bijus", "Outros"]};
  categoriaEscolhida='';
  subcategoriaEscolhida='';
  imageName='';
  imagesToUpload=[];
  private loading;
  constructor(private router: Router, private camera: Camera, private imagePicker: ImagePicker, 
    private imgService: ImageService, 
    private loadingCtrl: LoadingController, private dataService: DataService, private toastCtrl:ToastController) { }

  ngOnInit() {
    this.selected='upload';
    this.imageResponse=[];
  }

  takePicture() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetHeight: 400,
      targetWidth: 400
    }
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let dataURI= 'data:image/jpeg;base64,' + imageData;
      this.imageResponse.push(dataURI);
     }, (err) => {
      // Handle error
     });
  }
  galeria(tipo){
    if (tipo=='acessorios'){
      this.categoriasArray= ["Acessórios e cia"];
      this.categoriaEscolhida="Acessórios e cia";
    } else if (tipo=='roupas') {
      this.categoriasArray= ["Blusas e cia", "Calças e cia", "Vestidos e cia"];

    } else {
      this.categoriasArray= ["Sapatos e cia"];
      this.categoriaEscolhida="Sapatos e cia";
    }
    let options = {
        // max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      maximumImagesCount: 20,
      
      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      maxwidth: 400,
      height: 400,
      outputType: 1
    };

  this.imagePicker.getPictures(options).then((results) => {
      let dataURI;
      for (var i = 0; i < results.length; i++) {
        dataURI='data:image/jpeg;base64,' + results[i];
        this.imageResponse.push(dataURI);
      }
      console.log(this.imageResponse)
    }, (err) => {
      alert(err);
    });
    //this.router.navigate(['products']);
  }

  salvar() {

    let numImages=this.imageResponse.length;
    let currImage=1;
    this.presentLoading();
    this.imagesToUpload=[];
    this.imageResponse.forEach(image => {
      if (numImages==1) {
        this.imgService.images.push({data: image, categoria: this.categoriaEscolhida, 
          subcategoria: this.subcategoriaEscolhida, nome: this.imageName});
        this.imagesToUpload.push({data: image, categoria: this.categoriaEscolhida, 
          subcategoria: this.subcategoriaEscolhida, nome: this.imageName});
      } else {
        this.imgService.images.push({data: image, categoria: this.categoriaEscolhida, 
          subcategoria: this.subcategoriaEscolhida, nome: ""});
        this.imagesToUpload.push({data: image, categoria: this.categoriaEscolhida, 
          subcategoria: this.subcategoriaEscolhida, nome: ""});
      }
      console.log(this.imagesToUpload);
    });
    this.dataService.uploadImage(this.imagesToUpload).then(()=>{
    
      this.loading.dismiss();
      this.imageResponse=[];
      this.router.navigate(['home'])
    }).catch((err)=>{
      this.loading.dismiss();
      this.presentToast(err);
    });
    
  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Aguarde...' });
    return this.loading.present();
  }
  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }

  resizedataURL(datas, wantedWidth, wantedHeight){
    return new Promise(function(resolve,reject){

        // We create an image to receive the Data URI
        var img = document.createElement('img');

        // When the event "onload" is triggered we can resize the image.
        img.onload = function()
        {        
            // We create a canvas and get its context.
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');

            // We set the dimensions at the wanted size.
            canvas.width = wantedWidth;
            canvas.height = wantedHeight;

            // We resize the image with the canvas method drawImage();
            ctx.drawImage(img, 0, 0, wantedWidth, wantedHeight);

            var dataURI = canvas.toDataURL();

            // This is the return of the Promise
            resolve(dataURI);
        };

        // We put the Data URI in the image's src attribute
        img.src = datas;

    })
  }
}
