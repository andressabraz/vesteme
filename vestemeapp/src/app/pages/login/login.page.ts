import { DataService } from 'src/app/data.service';
import { Facebook } from '@ionic-native/facebook/ngx';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, LoadingController, ToastController } from '@ionic/angular';
import { User } from 'src/app/interfaces/user';
import { AuthService } from 'src/app/services/auth.service';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import * as firebase from 'firebase';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  @ViewChild(IonSlides) slides: IonSlides;
  public wavesPosition: number = 0;
  private wavesDifference: number = 100;
  public userLogin: User = {};
  public userRegister: User = {};
  private loading: any;

  constructor(
    private authService: AuthService,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public keyboard: Keyboard,
    private router: Router,
    public facebook: Facebook,
    public dataService: DataService
  ) { }

  ngOnInit() { }

  segmentChanged(event: any) {
    if (event.detail.value === 'login') {
      this.slides.slidePrev();
      this.wavesPosition += this.wavesDifference;
    } else {
      this.slides.slideNext();
      this.wavesPosition -= this.wavesDifference;
    }
  }

  async facebookLogin() {
    return this.facebook.login(['public_profile', 'email'])
    .then( response => {
      const facebookCredential = firebase.auth.FacebookAuthProvider
      .credential(response.authResponse.accessToken);
      this.facebook.api(response.authResponse.userID+"/?fields=id,name,email,gender", []).then((response)=>{
        this.presentLoading();
        let userInfo= {
          name: response['name'],
          email: response['email'],
          gender: response['gender'],
          birthdate: response['birthdate'],
          profession: response['profession'],
          city_state: response['city_state']
        }
        firebase.auth().signInWithCredential(facebookCredential)
        .then( success => { 
          this.dataService.createUser(userInfo);
          this.customLoading('Aguarde enquanto carregamos seu closet...');
          this.dataService.getImagesFromUser().then(()=>{
            this.router.navigate(['home']);
            this.loading.dismiss();
          });
          
          console.log("Firebase success: " + JSON.stringify(success)); 
        }).finally(()=>{this.loading.dismiss()});
      }).catch((err)=> {
        console.log(err)
      })
      
      
    }); 
  }

  async login() {
    await this.presentLoading();

    try {
      await this.authService.login(this.userLogin).then((user)=>{
        if (user.user.emailVerified) {
          this.customLoading('Aguarde enquanto carregamos seu closet...');
          this.dataService.getImagesFromUser().then(()=>{
            this.router.navigate(['home']);
            this.loading.dismiss();
          });
        } else {
          this.presentToast("Por favor verifique seu e-mail")
          this.authService.logout();
        }
      });
    } catch (error) {
      console.log(error.message)
      this.presentToast(error.message);
    } finally {
      this.loading.dismiss();
    }
  }

  async register() {
    await this.presentLoading();

    try {
      await this.authService.register(this.userRegister).then(()=> {
        let user = firebase.auth().currentUser;
        user.sendEmailVerification();
        this.dataService.createUser(this.userRegister);
        this.presentToast("Um e-mail de verificação foi enviado")
      });
    } catch (error) {
      this.presentToast(error.message);
    } finally {
      this.loading.dismiss();
    }
  }

  //abaixo segue a parte para aparecer uma mensagem de erro ao entrar com o email e senha e
  //aparece carregando quando estiver verificando email e senha

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Aguarde...' });
    return this.loading.present();
  }

  async customLoading(text) {
    this.loading = await this.loadingCtrl.create({ message: text });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }

}
