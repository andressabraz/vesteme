import { LookService } from './look.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { resolve } from 'url';
import { ImageService } from './image.service';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  usersCollection;
  constructor(private afs: AngularFirestore, public imageService: ImageService, public lookService: LookService) { 
    this.usersCollection = this.afs.collection('user');
  }

  getImagesFromUser() {
    let imageData = [];
    let storageRef = firebase.storage().ref('');
    let userId=firebase.auth().currentUser.uid;
    this.imageService.images=[];
    this.lookService.meusLooks=[];
    let _this=this;

    return new Promise((resolve, reject)=>{

        this.usersCollection.doc(userId).get().subscribe(data=>{
      if (data.data().roupas) {
      if (data.data().roupas.length==0) {
        resolve();
      }
      data.data().roupas.forEach(image => {
        storageRef.child(image.imageUrl).getDownloadURL().then(function(url) {
          let canvas = document.createElement("canvas");
          let context = canvas.getContext('2d');

          canvas.width=500;
          canvas.height=500;

          make_base();

          function make_base()
          { 
            let base_image = new Image();
            base_image.crossOrigin="anonymous" 
            base_image.src = url;
            base_image.onload = function(){
              context.drawImage(base_image,0,0);
              
              _this.imageService.images.push({"imageUrl": image.imageUrl, "data": canvas.toDataURL(), "categoria": image.categoria, "subcategoria": image.subcategoria, "nome": image.nome});
                resolve();
            }
          }
        })
      });
      } else if (data.data().looks) {
      data.data().looks.forEach(look => {
        storageRef.child(look.imageUrl).getDownloadURL().then(function(url) {
          let canvas = document.createElement("canvas");
          let context = canvas.getContext('2d');

          canvas.width=500;
          canvas.height=500;

          make_base();

          function make_base()
          {
            let base_image = new Image();
            base_image.crossOrigin="anonymous" 
            base_image.src = url;
            base_image.onload = function(){
              context.drawImage(base_image,0,0);
              
              _this.lookService.meusLooks.push({"data": canvas.toDataURL(), "imageUrl": look.imageUrl, "categoria": look.categoria, "nome": look.nome});
            }
          }
        })
      });
      } else {
        resolve();
      }
    });
    })
  }

  uploadLook(look) {
    function createUUID() {
      // http://www.ietf.org/rfc/rfc4122.txt
      var s = [];
      var hexDigits = "0123456789abcdef";
      for (var i = 0; i < 36; i++) {
          s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
      }
      s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
      s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
      s[8] = s[13] = s[18] = s[23] = "-";
  
      var uuid = s.join("");
      return uuid;
    }

    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref('');
      let userId=firebase.auth().currentUser.uid;
      
      let imageRef = storageRef.child('images').child(createUUID());
      imageRef.putString(look.data, 'data_url').then(snapshot => {
        this.usersCollection.doc(userId).update({
          
          looks: firebase.firestore.FieldValue.arrayUnion({"imageUrl": snapshot.metadata.fullPath, "categoria": look.categoria, "nome": look.nome ? look.nome:"", })
        });
        
        resolve();
      }, err=>{
        console.log(err);
        reject(err);
      })
    })
  }

  createUser(userInfo) {
    console.log(userInfo);
    let userId=firebase.auth().currentUser.uid;
    this.usersCollection.doc(userId).set({
      name: userInfo.name ? userInfo.name: "",
      email: userInfo.email ? userInfo.email: "",
      gender: userInfo.gender ? userInfo.gender: "",
      birthdate: userInfo.birthdate ? userInfo.birthdate: "",
      profession: userInfo.profession ? userInfo.profession: "",
      city_state: userInfo.city_state ? userInfo.city_state: ""
    }, {merge: true})
  }

  uploadImage(images) {
    function createUUID() {
      // http://www.ietf.org/rfc/rfc4122.txt
      var s = [];
      var hexDigits = "0123456789abcdef";
      for (var i = 0; i < 36; i++) {
          s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
      }
      s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
      s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
      s[8] = s[13] = s[18] = s[23] = "-";
  
      var uuid = s.join("");
      return uuid;
    }
    let imagesToUpload: number = images.length;
    let uploadedImages: number = 0;
    let userId=firebase.auth().currentUser.uid;
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref('');
      images.forEach(image => {
        let imageRef = storageRef.child('images').child(createUUID());
        imageRef.putString(image.data, 'data_url').then(snapshot => {
          uploadedImages++;
          this.usersCollection.doc(userId).update({
            
            roupas: firebase.firestore.FieldValue.arrayUnion({"imageUrl": snapshot.metadata.fullPath, "categoria": image.categoria, "subcategoria": image.subcategoria, "nome": image.nome ? image.nome:""})
          });
          
          if (imagesToUpload==uploadedImages) {
            resolve();
          }
        }, err=>{
          console.log(err);
          reject(err);
        })
        
      });
    })
  }

  async updateImage(image) {
    let userId=firebase.auth().currentUser.uid;
    return new Promise((resolve, reject)=>{

      this.usersCollection.doc(userId).get().subscribe(data=>{
        let list=data.data().roupas;
        let newList=[];
        list.forEach(element => {
          if (element.imageUrl==image.imageUrl) {
            newList.push({categoria: image.categoria, imageUrl: image.imageUrl, nome: image.nome, subcategoria: image.subcategoria})
          } else {
            newList.push(element)
          }
        });
        this.usersCollection.doc(userId).update({
          roupas: newList
        })
        resolve();
      });
    })
  }
  deleteImage(imageUrl) {
    let userId=firebase.auth().currentUser.uid;
    return new Promise((resolve, reject)=>{

      this.usersCollection.doc(userId).get().subscribe(data=>{
        let list=data.data().roupas;
        let newList=[];
        list.forEach(element => {
          if (element.imageUrl!=imageUrl) {
            newList.push(element)
          }
        });
        this.usersCollection.doc(userId).update({
          roupas: newList
        })
        resolve();
      });
    })
  }

  async updateLook(look) {
    let userId=firebase.auth().currentUser.uid;
    return new Promise((resolve, reject)=>{

      this.usersCollection.doc(userId).get().subscribe(data=>{
        let list=data.data().looks;
        let newList=[];
        list.forEach(element => {
          if (element.imageUrl==look.imageUrl) {
            newList.push({categoria: look.categoria, imageUrl: look.imageUrl, nome: look.nome})
          } else {
            newList.push(element)
          }
        });
        this.usersCollection.doc(userId).update({
          looks: newList
        })
        resolve();
      });
    })
  }
  deleteLook(imageUrl) {
    console.log(imageUrl);
    let userId=firebase.auth().currentUser.uid;
    return new Promise((resolve, reject)=>{

      this.usersCollection.doc(userId).get().subscribe(data=>{
        let list=data.data().looks;
        let newList=[];
        list.forEach(element => {
          if (element.imageUrl!=imageUrl) {
            console.log(element.imageUrl, imageUrl)
            newList.push(element)
          }
        });
        this.usersCollection.doc(userId).update({
          looks: newList
        })
        resolve();
      });
    })
  }
  
}
