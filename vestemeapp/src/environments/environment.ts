// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDUOTnbsZDecXo39PaB--9XGJh8jYLThsw",
    authDomain: "vesteme-79434.firebaseapp.com",
    databaseURL: "https://vesteme-79434.firebaseio.com",
    projectId: "vesteme-79434",
    storageBucket: "gs://vesteme-79434.appspot.com/",
    messagingSenderId: "42291599778",
    appId: "1:42291599778:web:ab946f1321f363bc"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
